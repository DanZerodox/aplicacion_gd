import 'package:aplicacion/Pages/Buscar.dart';
import 'package:aplicacion/Pages/Categorias.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';

import 'Models/custom_search.dart';
import 'Pages/Inicio.dart';
import 'Pages/Perfil.dart';

const primaryColor = Color.fromRGBO(5, 0, 102, 1);

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    var device = MediaQuery.of(context).size;
    final appBar = index == 1
        ? AppBar(
            title: const Text("G+D"),
            centerTitle: true,
            backgroundColor: primaryColor,
            actions: [
              IconButton(
                  onPressed: () {
                    showSearch(context: context, delegate: CustomSearchDelegate());
                  },
                  icon: Icon(Icons.search))
            ],
          )
        : AppBar(
            title: const Text("G+D"),
            centerTitle: true,
            backgroundColor: primaryColor,
          );

    return Scaffold(
      appBar: appBar,
      body: buildPages(),
      bottomNavigationBar: buildBottomNavigation(),
    );
  }

  Widget buildPages() {
    switch (index) {
      case 1:
        return BuscarPage();
      case 2:
        return CategoriasPage();
      case 3:
        return PerfilPage();
      case 0:
      default:
        return InicioPage();
    }
  }

  Widget buildBottomNavigation() {
    return BottomNavyBar(
      selectedIndex: index,
      onItemSelected: (index) => setState(() => this.index = index),
      items: <BottomNavyBarItem>[
        BottomNavyBarItem(icon: Icon(Icons.home), title: Text("Inicio")),
        BottomNavyBarItem(icon: Icon(Icons.search), title: Text("Directorio")),
        BottomNavyBarItem(icon: Icon(Icons.window), title: Text("Categorias")),
        BottomNavyBarItem(icon: Icon(Icons.person), title: Text("Perfil"))
      ],
    );
  }
}
