import 'dart:js';

import 'package:aplicacion/Pages/Horas.dart';
import 'package:flutter/material.dart';

class Option {
  Icon icon;
  String title;
  String subtitle;
  Option({required this.icon, required this.title, required this.subtitle});
}

final options = [
  Option(icon: Icon(Icons.calendar_today, size: 40.0), title: "Registro de horas", subtitle: "Mi bitacora de horas"),
  Option(icon: Icon(Icons.kitchen, size: 40.0), title: "Alimentos y bebidas", subtitle: "Mi gasto en comedor"),
  Option(icon: Icon(Icons.warning, size: 40.0), title: "Incidencias", subtitle: "Registro y seguimiento de mis incidencias"),
  Option(icon: Icon(Icons.credit_card, size: 40.0), title: "Nomina", subtitle: "Mi nomina"),
  Option(icon: Icon(Icons.money, size: 40.0), title: "Comprobante de Ingresos", subtitle: "Mi nomina"),
  Option(icon: Icon(Icons.card_membership, size: 40.0), title: "Solicitud de credencial", subtitle: "Mi nomina"),
];
