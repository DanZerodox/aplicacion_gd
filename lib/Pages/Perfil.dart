import 'package:flutter/material.dart';

class PerfilPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SafeArea(
            child: Column(children: [
      Container(
          decoration: BoxDecoration(image: DecorationImage(image: NetworkImage("https://www.merkur.de/bilder/2014/12/11/4528245/877884508-louisenthal-giesecke-2CXbQYfybz70.jpg"), fit: BoxFit.cover)),
          child: Container(
              width: double.infinity,
              height: 200,
              child: Container(
                  alignment: Alignment(0.0, 2.5),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage("https://www.advancedexcel.net/wp-content/uploads/2019/05/client-40.jpg"),
                    radius: 60.0,
                  )))),
      SizedBox(
        height: 60,
      ),
      Text("Daniel Mendoza Mancilla", style: TextStyle(fontSize: 18.0, color: Colors.black, letterSpacing: 2.0, fontWeight: FontWeight.w300)),
      SizedBox(
        height: 10,
      ),
      Text("CDMX, México", style: TextStyle(fontSize: 18.0, color: Colors.black, letterSpacing: 2.0, fontWeight: FontWeight.w300)),
      SizedBox(height: 10),
      Card(margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0), elevation: 2.0, child: Padding(padding: EdgeInsets.symmetric(vertical: 12, horizontal: 30), child: Text("Puesto", style: TextStyle(letterSpacing: 2.0, fontWeight: FontWeight.w300)))),
      SizedBox(height: 15),
      Text(
        "Desarrollador || Hub México",
        style: TextStyle(fontSize: 18.8, color: Colors.black, letterSpacing: 2.0, fontWeight: FontWeight.w300),
      ),
      Card(
          margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Expanded(
                    child: Column(children: [
                  Text("Extensión", style: TextStyle(color: Colors.blueAccent, fontSize: 22.0, fontWeight: FontWeight.w600)),
                  SizedBox(height: 7),
                  Text("3328", style: TextStyle(color: Colors.black, fontSize: 22.0, fontWeight: FontWeight.w300))
                ])),
                Expanded(
                  child: Column(
                    children: [
                      Text("Correo", style: TextStyle(color: Colors.blueAccent, fontSize: 22.0, fontWeight: FontWeight.w600)),
                      SizedBox(height: 7),
                      Text("mendozad", style: TextStyle(color: Colors.black, fontSize: 22.0, fontWeight: FontWeight.w300))
                    ],
                  ),
                )
              ])))
    ])));
  }
}
