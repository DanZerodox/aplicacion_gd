import 'package:aplicacion/Models/option_model.dart';
import 'package:flutter/material.dart';

import 'Cocina.dart';
import 'Horas.dart';

class CategoriasPage extends StatefulWidget {
  @override
  _CategoriasPageState createState() => _CategoriasPageState();
}

class _CategoriasPageState extends State<CategoriasPage> {
  int _selectedOption = 0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: ListView.builder(
          itemCount: options.length + 2,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return SizedBox(height: 15.0);
            } else if (index == options.length + 1) {
              return SizedBox(height: 100.0);
            }
            return Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(10.0),
              width: double.infinity,
              height: 80.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
                border: _selectedOption == index - 1 ? Border.all(color: Colors.lightBlueAccent) : null,
              ),
              child: ListTile(
                leading: options[index - 1].icon,
                title: Text(options[index - 1].title, style: TextStyle(color: _selectedOption == index - 1 ? Colors.blue : Colors.grey[600])),
                selected: _selectedOption == -1,
                iconColor: _selectedOption == index - 1 ? Colors.lightBlueAccent : null,
                onTap: () {
                  setState(() {
                    _selectedOption = index - 1;
                  });
                  if (index - 1 == 0) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => HorasPage()));
                  } else if (index - 1 == 1) {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CocinaPage()));
                  }
                },
              ),
            );
          }),
    );
  }
}
