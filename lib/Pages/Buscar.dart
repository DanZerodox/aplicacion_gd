import 'package:flutter/material.dart';

class BuscarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Container(
            padding: EdgeInsets.all(30.0),
            child: GridView.count(crossAxisCount: 2, children: <Widget>[
              Card(
                  child: InkWell(
                      onTap: () {},
                      child: Center(
                          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                        Icon(Icons.computer, size: 70),
                        Text("Sistemas")
                      ])))),
              Card(
                  child: InkWell(
                      onTap: () {},
                      child: Center(
                          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                        Icon(Icons.person, size: 70),
                        Text("Recursos Humanos")
                      ])))),
              Card(
                  child: InkWell(
                      onTap: () {},
                      child: Center(
                          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                        Icon(Icons.settings, size: 70),
                        Text("Ingenieria")
                      ])))),
              Card(
                  child: InkWell(
                      onTap: () {},
                      child: Center(
                          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                        Icon(Icons.kitchen, size: 70),
                        Text("Comedor")
                      ])))),
              Card(
                  child: InkWell(
                      onTap: () {},
                      child: Center(
                          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                        Icon(Icons.phone, size: 70),
                        Text("Recepción")
                      ])))),
              Card(
                  child: InkWell(
                      onTap: () {},
                      child: Center(
                          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                        Icon(Icons.security, size: 70),
                        Text("Seguridad")
                      ]))))
            ])));
  }
}
