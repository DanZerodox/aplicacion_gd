import 'package:aplicacion/UI/button_mini.dart';
import 'package:flutter/material.dart';

import '../UI/button.dart';

const primaryColor = Color.fromRGBO(5, 0, 102, 1);

class CocinaPage extends StatelessWidget {
  const CocinaPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: const Text("G+D"),
          centerTitle: true,
          backgroundColor: primaryColor,
        ),
        backgroundColor: const Color(0xffF6F6F9),
        body: Column(children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(height: 0),
                  IconButton(onPressed: () {}, icon: Icon(Icons.favorite_border_outlined))
                ],
              ))),
          Container(child: Image(image: NetworkImage("https://i.pinimg.com/originals/07/8f/fa/078ffafb3e52e3b5d20ccdba8d78635b.jpg")), width: 300, height: 280),
          Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
              ),
              width: MediaQuery.of(context).size.width,
              child: Center(
                  child: Column(children: <Widget>[
                const Padding(padding: EdgeInsets.only(top: 0), child: Text('Chilaquiles', style: TextStyle(fontSize: 18, fontFamily: 'Raleway', fontWeight: FontWeight.w600))),
                Container(alignment: Alignment.centerLeft, child: const Padding(padding: EdgeInsets.only(left: 39, top: 0, bottom: 0), child: Text('Acompañamiento', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 10, fontFamily: 'Raleway'), textAlign: TextAlign.left))),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const <Widget>[
                    ButtonMini(),
                    ButtonMini(color: 0xffA1C89B, text: 'Ensalada'),
                    ButtonMini(
                      color: 0xffC9A19C,
                      text: 'Fruta',
                    ),
                  ],
                ),
                Padding(padding: const EdgeInsets.only(top: 20, left: 45), child: Container(alignment: Alignment.centerLeft, child: const Text('Descripción', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w700, fontSize: 12)))),
                const Padding(padding: EdgeInsets.only(top: 5, left: 45), child: Text('Disfruta de este delicioso platillo acompañado de un huevo estrellado, queso y crema', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w400, fontSize: 12, color: Colors.black38))),
                Padding(
                    padding: const EdgeInsets.only(left: 53, right: 50, bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const <Widget>[
                        Text(
                          'Precio',
                          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w400, fontFamily: 'Raleway'),
                        ),
                        Text(
                          '\$35',
                          style: TextStyle(color: Color(0xff5956E9), fontSize: 22, fontWeight: FontWeight.w700, fontFamily: 'Raleway'),
                        ),
                      ],
                    )),
                Button(
                  text: 'Comprar',
                  color: Color(0xff5956E9),
                  width: 314,
                  onClick: () {},
                  height: 60,
                  fontSize: 20,
                )
              ])))
        ]));
  }
}
