import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const primaryColor = Color.fromRGBO(5, 0, 102, 1);

class HorasPage extends StatefulWidget {
  @override
  _HorasPageState createState() => _HorasPageState();
}

class _HorasPageState extends State<HorasPage> {
  List<Map> _horas = [
    {
      'fecha': '06/07/2022',
      'dia': 'Miercoles',
      'hora_entrada': '08:00 am',
      'hora_salida': '17:30 pm',
      'horas_total': '9.5 hrs'
    },
    {
      'fecha': '07/07/2022',
      'dia': 'Jueves',
      'hora_entrada': '08:00 am',
      'hora_salida': '17:30 pm',
      'horas_total': '9.5 hrs'
    },
    {
      'fecha': '08/07/2022',
      'dia': 'Viernes',
      'hora_entrada': '08:00 am',
      'hora_salida': '17:30 pm',
      'horas_total': '9.5 hrs'
    },
    {
      'fecha': '11/07/2022',
      'dia': 'Lunes',
      'hora_entrada': '08:00 am',
      'hora_salida': '----',
      'horas_total': '----'
    }
  ];

  int _currentSortColumn = 0;
  bool _isSortAsc = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("G+D"),
          centerTitle: true,
          backgroundColor: primaryColor,
        ),
        body: Column(
          children: <Widget>[
            SingleChildScrollView(scrollDirection: Axis.horizontal, child: _createDataTable()),
            Row(mainAxisAlignment: MainAxisAlignment.start, children: const <Widget>[
              Padding(padding: const EdgeInsets.only(top: 20, left: 2), child: Text('Horas Laboradas', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w700, fontSize: 12))),
              Padding(padding: const EdgeInsets.only(top: 20, left: 25), child: Text('Horas Meta', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w700, fontSize: 12))),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: const <Widget>[
              Padding(padding: const EdgeInsets.only(top: 20, left: 2), child: Text('25 Hrs', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w400, fontSize: 12))),
              Padding(padding: const EdgeInsets.only(top: 20, left: 25), child: Text('48 Hrs', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w400, fontSize: 12))),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: const <Widget>[
              Padding(padding: const EdgeInsets.only(top: 20, left: 2), child: Text('Faltas', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w700, fontSize: 12))),
              Padding(padding: const EdgeInsets.only(top: 20, left: 25), child: Text('Vacaciones', style: TextStyle(fontFamily: 'Raleway', fontWeight: FontWeight.w700, fontSize: 12))),
            ])
          ],
        ));
  }

  DataTable _createDataTable() {
    return DataTable(
      columns: _createColumns(),
      rows: _createRows(),
      sortColumnIndex: _currentSortColumn,
      sortAscending: _isSortAsc,
    );
  }

  List<DataColumn> _createColumns() {
    return [
      DataColumn(
          label: Text('Fecha'),
          onSort: (columnIndex, _) {
            setState(() {
              _currentSortColumn = columnIndex;
              if (_isSortAsc) {
                _horas.sort((a, b) => b['fecha'].compareTo(a['fecha']));
              } else {
                _horas.sort((a, b) => a['fecha'].compareTo(a['fecha']));
              }
              _isSortAsc = !_isSortAsc;
            });
          }),
      DataColumn(label: Text('Día')),
      DataColumn(label: Text('Hora Entrada')),
      DataColumn(label: Text('Hora Salida')),
      DataColumn(label: Text('Horas Totales')),
    ];
  }

  List<DataRow> _createRows() {
    return _horas
        .map((hora) => DataRow(cells: [
              DataCell(Text(hora['fecha'].toString())),
              DataCell(Text(hora['dia'].toString())),
              DataCell(Text(hora['hora_entrada'].toString())),
              DataCell(Text(hora['hora_salida'].toString())),
              DataCell(Text(hora['horas_total'].toString()))
            ]))
        .toList();
  }
}
