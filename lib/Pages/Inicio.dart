import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InicioPage extends StatelessWidget {
  const InicioPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var device = MediaQuery.of(context).size;
    return Scaffold(
      body: new ListView.builder(
          itemCount: 5,
          itemBuilder: (context, index) => index > 0
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
                      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Container(height: 40.0, width: 40.0, decoration: new BoxDecoration(shape: BoxShape.circle, image: new DecorationImage(fit: BoxFit.fill, image: new NetworkImage("https://www.itreseller.ch/imgserver/artikel/Personen/2020/mid/Andrej_Vckovski_und_Ralf_Winte_200908_140957.jpg")))),
                            new SizedBox(width: 10.0),
                            new Text(
                              "Comunicación Interna",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ]),
                    ),
                    Flexible(fit: FlexFit.loose, child: new Image.network("https://www.itreseller.ch/imgserver/artikel/Personen/2020/mid/Andrej_Vckovski_und_Ralf_Winte_200908_140957.jpg", fit: BoxFit.cover)),
                    Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                          new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                            new Text("Publicado el 08/07/2022", style: TextStyle(fontWeight: FontWeight.bold)),
                            new SizedBox(width: 56.0),
                          ]),
                          new Icon(FontAwesomeIcons.star),
                          new Text("12", style: TextStyle(fontWeight: FontWeight.bold)),
                          new Icon(FontAwesomeIcons.comment),
                          new Text("5", style: TextStyle(fontWeight: FontWeight.bold)),
                        ])),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                          new Container(height: 40.0, width: 40.0, decoration: new BoxDecoration(shape: BoxShape.circle, image: new DecorationImage(fit: BoxFit.fill, image: new NetworkImage("https://www.advancedexcel.net/wp-content/uploads/2019/05/client-40.jpg")))),
                          new SizedBox(width: 10.0),
                          Expanded(child: new TextField(decoration: new InputDecoration(border: InputBorder.none, hintText: "Agregar un comentario...")))
                        ]))
                  ],
                )
              : (new SizedBox(width: 10.0))),
    );
  }
}
