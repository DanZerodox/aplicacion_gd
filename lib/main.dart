import 'package:flutter/material.dart';
import 'package:aplicacion/UI/input_field.dart';
import 'package:aplicacion/home_page.dart';
import 'package:flutter/services.dart';

// void main() => runApp(const MyApp());
//
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp
  ]).then((_) {
    runApp(new MyApp());
  });
}

const primaryColor = Color(0xFF151026);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Iniciar Sesión',
      theme: ThemeData(
        primaryColor: primaryColor,
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Color.fromRGBO(5, 0, 102, 1),
            child: Stack(children: <Widget>[
              Align(
                  alignment: Alignment.bottomRight,
                  heightFactor: 0.5,
                  widthFactor: 0.5,
                  child: Material(
                      borderRadius: const BorderRadius.all(Radius.circular(200.0)),
                      color: const Color.fromRGBO(255, 255, 255, 0.4),
                      child: Container(
                        width: 400,
                        height: 400,
                      ))),
              Center(
                child: Container(
                    width: 400,
                    height: 400,
                    child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
                      Material(
                          elevation: 10.0,
                          // borderRadius: const BorderRadius.all(Radius.circular(50.0)),
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset(
                                "images/logo.jpg",
                                width: 80,
                                height: 80,
                              ))),
                      Form(child: InputField(const Icon(Icons.person, color: Colors.white), "Usuario")),
                      Form(child: InputField(const Icon(Icons.lock, color: Colors.white), "Contraseña")),
                      Container(
                          width: 150,
                          child: ButtonTheme(
                              height: 70.0,
                              child: RaisedButton(
                                  onPressed: () {
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomePage()));
                                  },
                                  color: Colors.indigo,
                                  textColor: Colors.white,
                                  child: const Text(
                                    "Ingresar",
                                    style: TextStyle(fontSize: 20.0),
                                  ),
                                  shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))))))
                    ])),
              )
            ])));
  }
}
